[1mdiff --git a/src/views/manager/list.vue b/src/views/manager/list.vue[m
[1mindex 8ff73c5..cbfe8c3 100644[m
[1m--- a/src/views/manager/list.vue[m
[1m+++ b/src/views/manager/list.vue[m
[36m@@ -131,7 +131,7 @@[m
   </el-card>[m
 </template>[m
 <script setup>[m
[31m-import { ref, reactive, computed, onMounted } from 'vue'[m
[32m+[m[32mimport { ref } from 'vue'[m
 import FormDrawer from '@/components/FormDrawer.vue'[m
 import AddHeader from '@/components/AddHeader.vue'[m
 import {[m
[36m@@ -141,7 +141,6 @@[m [mimport {[m
   deleteManager,[m
   updateManagerStatus,[m
 } from '@/api/manager.js'[m
[31m-import { toast } from '@/composables/util'[m
 import { useInitTable, useInitForm } from '@/composables/useCommon'[m
 [m
 const roles = ref([])[m
[36m@@ -216,4 +215,8 @@[m [mconst updataStatus = (status, id) => {[m
     getData(currentPage.value)[m
   })[m
 }[m
[32m+[m
[32m+[m[32m// let n = ref(1)[m
[32m+[m[32m// const { add } = useAdd()       // n++[m
[32m+[m[32m// const { reduce } = useReduce() // n--[m
 </script>[m
