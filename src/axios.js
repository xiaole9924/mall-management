import axios from 'axios'
import { toast } from '@/composables/util.js'
import { getToken } from '@/composables/auth.js'
const service = axios.create({
  baseURL: '/api',
})

service.interceptors.request.use(
  (config) => {
    const token = getToken()
    if (token) {
      config.headers['token'] = token
    }
    return config
  },
  (err) => Promise.reject(err)
)

service.interceptors.response.use(
  (response) => response.data,
  (err) => {
    toast('error 500', 'error')

    return Promise.reject(err)
  }
)

export default service
