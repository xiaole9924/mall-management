import { ElNotification, ElMessageBox } from 'element-plus'
import nprogress from 'nprogress'

export const toast = (message, type = 'success') => {
  ElNotification({
    message,
    type,
    duration: 2000,
  })
}

export const showModal = (content = 'content', type = 'warning', title) => {
  return ElMessageBox.confirm(content, type, {
    confirmButtonText: 'OK',
    cancelButtonText: 'Cancel',
    type,
  })
}

export const showFullLoading = () => {
  nprogress.start()
}

export const hideFullLoading = () => {
  nprogress.done()
}

// 弹出输入框
export function showPrompt(tip, value = '') {
  return ElMessageBox.prompt(tip, '', {
    confirmButtonText: '确认',
    cancelButtonText: '取消',
    inputValue: value,
  })
}

// 将query对象转成url参数
export function queryParams(query) {
  let q = []
  for (const key in query) {
    if (query[key]) {
      q.push(`${key}=${encodeURIComponent(query[key])}`)
    }
  }
  let r = q.join('&')
  r = r ? '?' + r : ''
  return r
}
